import os
import sys
import re
from datetime import datetime
import configparser
import click
from click2cwl import dump
from pystac import Catalog, CatalogType
from shutil import move, rmtree
from loguru import logger

from .stachelp import get_item


@click.command(
    short_help="{{cookiecutter.project_title}}",
    help="{{cookiecutter.project_description}}",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input_path",
    "-i",
    "input_path",
    help="Input product path (containing the catalog.json file)",
    type=click.Path(),
    required=True,
)
@click.option(
    "--aoi",
    "-a",
    "aoi",
    help="Area of interest in Well-known Text (WKT)",
    required=False,
    default=None,
)
@click.pass_context
def main(ctx, input_path, **kwargs):

    dump(ctx)

    item = get_item(os.path.join(input_path, "catalog.json"))

    # To BE set before logging anything

    logger.info("Starting process")

    logger.info("Put here your processor call")
    
    # something like:
    # item_out = my_processor(item, **kwargs)
    
    cat = Catalog(id="catalog", description="My processor output")

    cat.add_items([item_out])

    cat.normalize_and_save(root_href="./", catalog_type=CatalogType.SELF_CONTAINED)

    if "TMPDIR" in os.environ:
        move("catalog.json", os.path.join(cwd, "catalog.json"))
        move(item_out.id, os.path.join(cwd, item_out.id))

    logger.info(f"{item.id} processed")

    sys.exit(0)


if __name__ == "__main__":
    main()
