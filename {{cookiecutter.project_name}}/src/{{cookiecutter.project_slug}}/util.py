import sys
from loguru import logger
import traceback

def handle_unhandled_exception(*exc_info):
    text = "".join(traceback.format_exception(*exc_info))
    logger.error(f"Unhandled exception: {text}")
    sys.exit(255)


sys.excepthook = handle_unhandled_exception